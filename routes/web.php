<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('greet', function () {
    return '<h2>Hello world!</h1>';
});
$greet = function (){
  return '<h2>Hello world!</h1>';
};
Route::get ('greet', $greet);
